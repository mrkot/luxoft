package com.luxoft.bankapp.service.network.client;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ClientConfig {
    private static String SERVER_HOST;
    private static String PROPERTIES_FILE = "./client.conf";

    private static Properties properties = new Properties();
    private static FileInputStream propertiesFile = null;

    static {
        try {
            propertiesFile = new FileInputStream(PROPERTIES_FILE);

            properties.load(propertiesFile);

            SERVER_HOST = properties.getProperty("SERVER");
        } catch (FileNotFoundException e) {
            System.err.println("server.conf не найден");
        } catch (IOException e) {
            System.err.println("Ошибка чтения файла");
        } finally {
            try {
                propertiesFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static String getServerIp() {
        return SERVER_HOST;
    }
}
