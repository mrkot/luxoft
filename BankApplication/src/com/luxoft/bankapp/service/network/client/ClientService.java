package com.luxoft.bankapp.service.network.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientService {

    public static String userInput() {

        String userInput = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            userInput = br.readLine();
        } catch (IOException e) {
            System.out.println("Ошибка ввода");
        }
        return userInput;
    }
}
