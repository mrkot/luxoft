package com.luxoft.bankapp.service.network.client;

import java.net.*;
import java.io.*;

public class BankClient {
    private static final int PORT = 8080;
    private static ObjectOutputStream out;
    private static ObjectInputStream in;
    private static String serverAnsver = "NO";

    private static void sentMessage(final String message) {
        try {
            out.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String readMessage() {
        String str = null;
        try {
            str = (String) in.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        InetAddress ipAddress = InetAddress.getByName(ClientConfig.getServerIp());

        Socket socket = new Socket(ipAddress, PORT);
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(socket.getInputStream());

            //ENTER BANK (BANK_NAME)
            do {
                AtmMenu.printBankNameMenu();
                sentMessage("BANK_NAME");
                sentMessage(ClientService.userInput());
                serverAnsver = readMessage();
            } while (!serverAnsver.equalsIgnoreCase("OK"));

            //ENTER NAME (FULL_NAME)
            do {
                AtmMenu.printUserNameMenu();
                sentMessage("USER_NAME");
                sentMessage(ClientService.userInput());
                serverAnsver = readMessage();
            } while (!serverAnsver.equalsIgnoreCase("OK"));

            //SHOW BALANCE
            do {
                AtmMenu.printUserAccountsMenu();
                sentMessage("ACCOUNT");
                serverAnsver = readMessage();
                System.out.println(serverAnsver);

                serverAnsver = readMessage();
            } while (!serverAnsver.equalsIgnoreCase("OK"));

            //SELECT ACCOUNT
            do {
                AtmMenu.printUserSelectAccountMenu();
                sentMessage("SELECT_USER_ACCOUNT");
                sentMessage(ClientService.userInput());

                serverAnsver = readMessage();
                System.out.println(serverAnsver); //выбран аккаунт

                AtmMenu.printUserWithdrowMenu();
                sentMessage(ClientService.userInput());

                serverAnsver = readMessage();
                System.out.println(serverAnsver);

                serverAnsver = readMessage();
            } while (!serverAnsver.equalsIgnoreCase("OK"));

            out.writeObject("END_SESSION");
        } finally {
            System.out.println("END_SESSION");
            socket.close();
        }
    }
}
