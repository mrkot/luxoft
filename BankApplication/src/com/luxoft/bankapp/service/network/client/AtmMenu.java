package com.luxoft.bankapp.service.network.client;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class AtmMenu {
    private static String BANK_NAME = "./menu/bankName.conf";
    private static String USER_NAME = "./menu/userName.conf";
    private static String USER_ACCOUNTS = "./menu/userAccounts.conf";
    private static String USER_WITHDROW = "./menu/userWithdrow.conf";


    private static Properties properties = new Properties();
    private static FileInputStream propertiesFile = null;

    public static void printBankNameMenu() {
        try {
            propertiesFile = new FileInputStream(BANK_NAME);

            properties.load(propertiesFile);

            System.out.print(properties.getProperty("BANK_NAME"));

        } catch (FileNotFoundException e) {
            System.err.println("server.conf не найден");
        } catch (IOException e) {
            System.err.println("Ошибка чтения файла");
        } finally {
            try {
                propertiesFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void printUserNameMenu() {
        try {
            propertiesFile = new FileInputStream(USER_NAME);

            properties.load(propertiesFile);

            System.out.print(properties.getProperty("USER_NAME"));

        } catch (FileNotFoundException e) {
            System.err.println("server.conf не найден");
        } catch (IOException e) {
            System.err.println("Ошибка чтения файла");
        } finally {
            try {
                propertiesFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void printUserAccountsMenu() {
        try {
            propertiesFile = new FileInputStream(USER_ACCOUNTS);

            properties.load(propertiesFile);

            System.out.print(properties.getProperty("USER_ACCOUNTS"));

        } catch (FileNotFoundException e) {
            System.err.println("server.conf не найден");
        } catch (IOException e) {
            System.err.println("Ошибка чтения файла");
        } finally {
            try {
                propertiesFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void printUserSelectAccountMenu() {
        try {
            propertiesFile = new FileInputStream(USER_WITHDROW);

            properties.load(propertiesFile);

            System.out.print(properties.getProperty("USER_SELECT_ACCOUNT"));

        } catch (FileNotFoundException e) {
            System.err.println("server.conf не найден");
        } catch (IOException e) {
            System.err.println("Ошибка чтения файла");
        } finally {
            try {
                propertiesFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void printUserWithdrowMenu() {
        try {
            propertiesFile = new FileInputStream(USER_WITHDROW);

            properties.load(propertiesFile);

            System.out.print(properties.getProperty("USER_WITHDROW"));

        } catch (FileNotFoundException e) {
            System.err.println("server.conf не найден");
        } catch (IOException e) {
            System.err.println("Ошибка чтения файла");
        } finally {
            try {
                propertiesFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


}
