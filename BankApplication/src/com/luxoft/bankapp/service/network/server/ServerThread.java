package com.luxoft.bankapp.service.network.server;

import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.CheckingAccount;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.SavingAccount;
import com.luxoft.bankapp.exception.BankNotFoundException;
import com.luxoft.bankapp.exception.ClientNotFound;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.exception.OverDraftLimitExceededException;
import com.luxoft.bankapp.service.bank.BankCommander;
import com.luxoft.bankapp.service.command.DBSelectBankCommander;
import com.luxoft.bankapp.service.command.DBSelectClientCommander;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerThread implements Runnable {

    private Socket connect = null;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    private static void showConnectInfo(Socket connect) {
        System.out.println("----------------------------------------");
        System.out.println("|   Incoming connection from:");
        System.out.println("|   Remote IP:      " + connect.getInetAddress());
        System.out.println("|   Remote port:    " + connect.getPort());
        System.out.println("|   On local port:  " + connect.getLocalPort());
        System.out.println("----------------------------------------");
    }

    public ServerThread(Socket connect) throws IOException {
        this.connect = connect;

        out = new ObjectOutputStream(connect.getOutputStream());
        out.flush();
        in = new ObjectInputStream(connect.getInputStream());
        showConnectInfo(connect);
        // run();
    }

    @Override
    public void run() {
        BankServerThreaded.weitingClientsCounter.decrementAndGet();
        try {
            while (true) {
                String str = (String) in.readObject();

                //SELECT BANK
                if (str.equals("BANK_NAME")) {
                    str = (String) in.readObject();

                    DBSelectBankCommander.getInstance().execute(str);

                    if (BankCommander.activebank != null) {
                        out.writeObject("OK");
                        System.out.println("Set active bank: " + BankCommander.activebank.getBankName());
                    } else {
                        out.writeObject("ERROR");
                    }
                    str = "";
                }

                //SELECT USER
                if (str.equals("USER_NAME")) {
                    str = (String) in.readObject();

                    DBSelectClientCommander.getInstance().execute(BankCommander.activebank, str);

                    if (BankCommander.activeClient != null) {
                        out.writeObject("OK");
                        System.out.println("Set active client: " + BankCommander.activeClient.getFullName());
                    } else {
                        out.writeObject("ERROR");
                    }
                    str = "";
                }

                //SHOW ACCOUNTS INFO
                if (str.equals("ACCOUNT")) {
                    String accountInfo = "";

                    if (BankCommander.activeClient != null) {

                        for (Account acc : BankCommander.activeClient.getAccounts()) {

                            if (acc instanceof SavingAccount) {
                                accountInfo += "Saving account\n";
                                accountInfo += "Balance: " + ((SavingAccount) acc).getBalance() + "\n";
                            } else {
                                accountInfo += "Checking account\n";
                                accountInfo += "Balance: " + ((CheckingAccount) acc).getBalance() + "\n";
                                accountInfo += "Overdraft: " + ((CheckingAccount) acc).getOverdraft() + "\n";
                            }
                            accountInfo += "********************\n";
                        }
                    }
                    str = "";
                    out.writeObject(accountInfo);
                    out.writeObject("OK");
                }

                //WITHDROW
                if (str.equals("SELECT_USER_ACCOUNT")) {

                    String accountType = (String) in.readObject();

                    if (accountType.equalsIgnoreCase("C")) {

                        for (Account acc : BankCommander.activeClient.getAccounts()) {
                            if (acc.getAccountType().equalsIgnoreCase("C")) {
                                Client.activeDbAccount = acc;
                                out.writeObject("SELECTED CHECKING ACCOUNT\n");
                            }
                        }
                    }

                    if (accountType.equalsIgnoreCase("S")) {

                        for (Account acc : BankCommander.activeClient.getAccounts()) {
                            if (acc.getAccountType().equalsIgnoreCase("S")) {
                                Client.activeDbAccount = acc;
                                out.writeObject("SELECTED SAVING ACCOUNT\n");
                            }
                        }
                    }

                    String amountMoneyWithdrow = (String) in.readObject();
                    double amount = Double.parseDouble(amountMoneyWithdrow);
                    Client.activeDbAccount.withdrawFromAccount(amount);

                    String accountInfo = "";
                    if (BankCommander.activeClient != null) {
                        for (Account acc : BankCommander.activeClient.getAccounts()) {

                            if (acc instanceof SavingAccount) {
                                accountInfo += "Saving account\n";
                                accountInfo += "Balance: " + ((SavingAccount) acc).getBalance() + "\n";
                            } else {
                                accountInfo += "Checking account\n";
                                accountInfo += "Balance: " + ((CheckingAccount) acc).getBalance() + "\n";
                                accountInfo += "Overdraft: " + ((CheckingAccount) acc).getOverdraft() + "\n";
                            }
                            accountInfo += "********************\n";
                        }
                    }
                    out.writeObject(accountInfo);
                    out.writeObject("OK");
                }

                //END SESSION
                if (str.equals("END_SESSION")) break;
                System.out.println("SysLog: " + str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (BankNotFoundException e) {
            e.printStackTrace();
        } catch (ClientNotFound clientNotFound) {
            clientNotFound.printStackTrace();
        } catch (OverDraftLimitExceededException e) {
            e.printStackTrace();
        } catch (NotEnoughFundsException e) {
            e.printStackTrace();
        } finally {
            try {
                connect.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
