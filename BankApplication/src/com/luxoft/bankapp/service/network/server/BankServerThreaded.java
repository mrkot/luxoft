package com.luxoft.bankapp.service.network.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class BankServerThreaded {

    public static AtomicInteger weitingClientsCounter = new AtomicInteger(0);

    public static void main(String[] args) throws IOException {


        ServerSocket serverSocket = new ServerSocket(ServerConfig.getServerPort());
        ExecutorService pool = Executors.newFixedThreadPool(2);
        ServerInfo.printInfo();

        try {

            Thread monitor = new Thread(new BankServerMonitor());
            monitor.setDaemon(true);
            monitor.start();

            while (true) {
                Socket connect = serverSocket.accept();
                weitingClientsCounter.incrementAndGet();
                pool.execute(new ServerThread(connect));
                //pool.execute(new BankServerMonitor());
            }
        } finally {
            serverSocket.close();
        }
    }
}
