package com.luxoft.bankapp.service.network.server; /**
 * Created by Константин on 28.02.14.
 */

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;


public class ServerInfo {

    public static void printInfo() throws UnknownHostException {
        InetAddress inetAddr = InetAddress.getLocalHost();

        System.out.println("***************************************");
        System.out.println("**   Started on:  " + inetAddr.getHostName());
        System.out.println("**   IP Addreses: " + inetAddr.getHostAddress());
        System.out.println("***************************************");
    }

}


