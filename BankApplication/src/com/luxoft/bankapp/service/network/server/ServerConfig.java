package com.luxoft.bankapp.service.network.server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ServerConfig {

    private static int SERVER_PORT;
    private static String PROPERTIES_FILE = "server.conf";

    private static Properties properties = new Properties();
    private static FileInputStream propertiesFile = null;

    static {
        try {
            propertiesFile = new FileInputStream(PROPERTIES_FILE);

            properties.load(propertiesFile);

            SERVER_PORT = Integer.parseInt(properties.getProperty("PORT"));
        } catch (FileNotFoundException e) {
            System.err.println("server.conf не найден");
        } catch (IOException e) {
            System.err.println("Ошибка чтения файла");
        } finally {
            try {
                propertiesFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static int getServerPort() {
        return SERVER_PORT;
    }

}

