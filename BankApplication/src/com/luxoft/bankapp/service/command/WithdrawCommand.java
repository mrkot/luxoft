package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.service.bank.BankService;

public class WithdrawCommand implements Command {
    @Override
    public void execute() {
        BankService.getInstance().withdrawCommand();
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Снятие средств со счета");
    }
}
