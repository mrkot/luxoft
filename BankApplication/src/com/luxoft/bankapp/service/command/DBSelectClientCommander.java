package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.dao.ClientDaoImpl;
import com.luxoft.bankapp.exception.ClientNotFound;

public class DBSelectClientCommander {

    private static DBSelectClientCommander instance;

    private DBSelectClientCommander() {
    }

    public static DBSelectClientCommander getInstance() {
        if (instance == null) {
            instance = new DBSelectClientCommander();
        }
        return instance;
    }

    public void execute(Bank bank, String name) throws ClientNotFound {
        ClientDaoImpl.getInstance().findClientByName(bank, name);
    }
}
