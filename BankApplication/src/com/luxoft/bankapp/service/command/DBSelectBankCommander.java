package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.domain.dao.BankDaoImpl;
import com.luxoft.bankapp.exception.BankNotFoundException;

public class DBSelectBankCommander {

    private static DBSelectBankCommander instance;

    private DBSelectBankCommander() {
    }

    public static DBSelectBankCommander getInstance() {
        if (instance == null) {
            instance = new DBSelectBankCommander();
        }
        return instance;
    }

    public void execute(String name) throws BankNotFoundException {
        BankDaoImpl.getInstant().getBankByName(name);
    }
}
