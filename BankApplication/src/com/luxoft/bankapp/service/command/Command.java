package com.luxoft.bankapp.service.command;

import java.io.IOException;

public interface Command {
    void execute() throws IOException;

    void printCommandInfo();
}



/*package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.Gender;
import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.exception.OverDraftLimitExceededException;
import com.luxoft.bankapp.service.bank.BankService;
import com.luxoft.bankapp.service.bank.BankCommander;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

 public final class BankApplication {


}

        try{
            BankService bankService = new BankService();
            Bank bankDnepr = new Bank("Днепропетровская филия");

            //                          Name        Surename    idPassport
            Client client1 = new Client("Bill",     "Cly",      "NB78I9",       Gender.MALE);
            Client client2 = new Client("Ann",      "Duter",    "JU1177",       Gender.FEMALE);
            Client client3 = new Client("Gonta",    "Juba",     "666547889XL",  Gender.FEMALE);
           // Client client4 = new Client("Bil",      "Cly",      "nb78i9",       Gender.MALE);

            bankService.addClient(bankDnepr, client1);
            bankService.addClient(bankDnepr, client2);
            bankService.addClient(bankDnepr, client3);
            //bankService.addClient(bankDnepr, client4);

            Account accountCheckingClient1 = client1.createAccount("checking");
            Account accountSavingClient1   = client1.createAccount("saving");
            Account accountSavingClient2   = client2.createAccount("saving");
            Account accountCheckingClient2   = client2.createAccount("checking");


            System.out.println(accountCheckingClient1.equals(accountSavingClient1));
            System.out.println(accountCheckingClient1.equals(accountSavingClient2));
            System.out.println(accountCheckingClient1.equals(accountCheckingClient2));



            client1.setActiveAccount(accountCheckingClient1);
            bankService.printBalance(client1);
            bankService.modifyBankDeposit(client1, 100);
            bankService.modifyBankDeposit(client1, 600);
            bankService.modifyBankWithdrow(client1, 50);

            client1.setActiveAccount(accountSavingClient1);
            bankService.printBalance(client1);
            bankService.modifyBankDeposit(client1, 300);
            bankService.modifyBankWithdrow(client1, 200);

            client2.setActiveAccount(accountSavingClient2);
            bankService.printBalance(client2);
            bankService.modifyBankDeposit(client2, 100);
            bankService.modifyBankWithdrow(client2, 50);

       }catch (OverDraftLimitExceededException e){
            System.out.println(e);
        }catch (NotEnoughFundsException e){
            System.out.println(e);
        }catch (ClientExistsException e){
            System.out.println(e);
        }catch (Exception e){
            e.printStackTrace();
        }*/
