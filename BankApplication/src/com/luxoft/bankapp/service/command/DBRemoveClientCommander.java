package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.dao.ClientDaoImpl;


public class DBRemoveClientCommander {

    public void execute(Client client) {
        ClientDaoImpl.getInstance().remove(client);
    }
}
