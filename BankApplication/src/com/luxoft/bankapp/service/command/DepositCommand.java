package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.service.bank.BankService;

public class DepositCommand implements Command {
    @Override
    public void execute() {
        BankService.getInstance().depositCommand();
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Пополнить счет");
    }
}
