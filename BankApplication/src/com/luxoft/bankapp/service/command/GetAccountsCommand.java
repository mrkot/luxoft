package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.service.bank.BankService;

public class GetAccountsCommand implements Command {
    @Override
    public void execute() {
        BankService.getInstance().getAccountCommand();
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Вывести список счетов клиента и остаток на счетах");
    }
}
