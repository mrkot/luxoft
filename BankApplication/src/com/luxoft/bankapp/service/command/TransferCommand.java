package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.service.bank.BankService;

public class TransferCommand implements Command {
    @Override
    public void execute() {
        BankService.getInstance().transferCommand();
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Перевод со счета клиента на счет другого клиента банка");
    }
}
