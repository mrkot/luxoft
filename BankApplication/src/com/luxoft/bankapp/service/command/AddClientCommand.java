package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.service.bank.BankService;

public class AddClientCommand implements Command {

    @Override
    public void execute() {
        try {
            BankService.getInstance().addClient();
        } catch (ClientExistsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Добавить клиента");
    }
}
