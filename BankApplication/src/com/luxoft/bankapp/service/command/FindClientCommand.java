package com.luxoft.bankapp.service.command;

import com.luxoft.bankapp.service.bank.BankService;

public class FindClientCommand implements Command {
    @Override
    public void execute() {
        BankService.getInstance().findClient();
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Поиск клиента");
    }
}
