package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.CheckingAccount;
import com.luxoft.bankapp.domain.bank.Client;

import java.util.*;

public class BankReport {

    public void getNumberOfClients(Bank bank) {
        System.out.println("Количество клиентов: " + bank.getClients().size());
    }

    public void getAccountsNumber(Bank bank) {
        int amountAccounts = 0;

        for (Client c : bank.getClients()) {
            amountAccounts += c.getAccounts().size();
        }
        System.out.println("Общее количество счетов клиентов: " + amountAccounts);
    }

    public void getClientsSorted(Bank bank) {
        System.out.print("Список всех клиентов, отсортированный по текущему остатку счета: ");

        Set<Client> set = new TreeSet<Client>();
        set.addAll(bank.getClients());
        for (Client client : set) {
            System.out.println(client.getFullName() + " " + client.getBalance() + "  ");
        }
    }

    public void getBankCreditSum(Bank bank) {

        double dept = 0;

        for (Client c : bank.getClients()) {
            for (Account acc : c.getAccounts()) {
                if (acc instanceof CheckingAccount) {
                    dept += ((CheckingAccount) acc).getOverdraft() - ((CheckingAccount) acc).getInitialOverdraft();
                }
            }
        }
        System.out.println("Общий долг составляет: " + dept);
    }

    public void getClientsByCity(Bank bank) {
        Map<String, List<Client>> cityByEachClient = new HashMap<String, List<Client>>();
        String city;

        for (Client c : bank.getClients()) {

            city = c.getCityClient();

            if (cityByEachClient.containsKey(city)) {  //есть ли такой город?

                List<Client> clientsList = new ArrayList<Client>();

                clientsList.addAll(cityByEachClient.get(city));
                clientsList.add(c);

                cityByEachClient.put(city, clientsList);

            } else {
                List<Client> clientsList = new ArrayList<Client>();
                clientsList.add(c);
                cityByEachClient.put(city, clientsList);
            }
        }
    }
}
