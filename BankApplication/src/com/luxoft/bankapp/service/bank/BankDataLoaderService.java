package com.luxoft.bankapp.service.bank;

import java.io.*;
import java.util.HashMap;
import java.util.Map;


public class BankDataLoaderService {

    private final String FEEDS_FOLDER = "feeds/";
    //Bank b = new Bank("sdfs");


    public void loadFeed(String folder) throws IOException {


        //Получаем все имена файлов в директории
        File dir = new File(folder);
        File file;
        String[] files = dir.list();

        for (int i = 0; i < files.length; i++) {

            //Получаем конкретный файл из массива
            file = new File(FEEDS_FOLDER + files[i]);
            BufferedReader br = new BufferedReader(new FileReader(file));

            //Вычитываем строчку с файла с параметрами клиента
            String s = br.readLine();
            br.close();

            //Получаем массив строк <K,V>
            String[] params = s.split(";");

            Map<String, String> map = new HashMap<String, String>();
            //Выделяем ключ и значение
            for (int j = 0; j < (params.length - 1); j++) {
                String[] kv = params[j].split("=");

                map.put(kv[0], kv[1]);
            }

            //BankCommander.activebank = b;
            BankCommander.activebank.parseFeed(map);

        }
    }
}
