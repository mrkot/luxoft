package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.Gender;
import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.exception.OverDraftLimitExceededException;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class BankService {

    /////////////////////////////////////////////////////////////////////
    ////                FIELDS
    /////////////////////////////////////////////////////////////////////
    public static Bank bank = new Bank("ДНЕПР");
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    Client c;

    final boolean isProcess = true;
    private String fullNameClient;
    private String idPassport;
    private String emailClient;
    private String phoneNumberClient;
    private double initOverdraft = 0.0;
    private Gender gender;

    private String patternNameClient = "^([А-ЯA-Z]{1})([а-яa-z]{2,}) ([А-ЯA-Z]{1})([а-яa-z]{2,}) ([А-ЯA-Z]{1})([а-яa-z]{2,})$";
    private String patternEmailClient = "^([A-Za-z\\.-0-9]{2,})@([A-Za-z\\.-0-9]{2,})\\.([A-Za-z]{2,3})$";
    private String patternPhoneNumberClient = "^(\\(?\\+38\\)?\\(?0[0-9]{2}\\)?[\\- ]?\\)?[0-9]{3}[\\- ]?[0-9]{2}[\\- ]?[0-9]{2})$";
    private String patternInitOverdraft = "^(\\d\\d?\\d?\\d?\\d?)\\.(\\d?\\d?)$";
    private String patternIdPassport = "";
    private final String directory = "serializable/";

    private static BankService instance;


    private BankService() {
    }

    public static BankService getInstance() {
        if (instance == null) {
            instance = new BankService();
        }
        return instance;
    }

    ///////////////////////////////////////////////////////////////
    ///                 Serializable
    ///////////////////////////////////////////////////////////////


    public void saveClient(Client client) {
        try {
            File dir = new File(directory);
            if (!dir.exists()) dir.mkdir();

            File file = new File(directory + client.getFullName());
            file.createNewFile();

            ObjectOutputStream objectOutputStream =
                    new ObjectOutputStream(new FileOutputStream(file));

            objectOutputStream.writeObject(client);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Client loadClient(String fullName) {
        Client client = null;
        try {
            File file = new File(directory + fullName);

            ObjectInputStream objectInputStream =
                    new ObjectInputStream(new FileInputStream(file));

            client = (Client) objectInputStream.readObject();
            objectInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return client;
    }
    ///////////////////////////////////////////////////////////////
    ///                 METHODS FOR PATTERN COMMAND
    ///////////////////////////////////////////////////////////////

    //*******************
    //  AddClientCommand
    //*******************

    public void addClient() throws ClientExistsException {
        fullNameClient = inputFullNameClient();
        idPassport = inputIdPassport();
        gender = inputGender();
        emailClient = inputEmailClient();
        phoneNumberClient = inputPhoneNumberClient();
        initOverdraft = inputInitOverdraft();

        c = new Client(fullNameClient, gender, emailClient, phoneNumberClient, idPassport, initOverdraft);
        bank.addClient(c);
        c.createAccount("checking");

    }

    private String inputFullNameClient() {
        String tmp = null;
        while (isProcess) {
            System.out.println("Ввеедите полное имя клиента (Ф И О)\n--> ");
            try {
                tmp = br.readLine();
            } catch (IOException e) {
                System.out.println("Введены допустимые символы");
            }
            if (checkRightInput(tmp, patternNameClient)) return tmp;
        }
    }

    private String inputIdPassport() {
        String tmp = null;
        while (isProcess) {
            System.out.println("Введите серию пасспорта\n--> ");
            try {
                tmp = br.readLine();
            } catch (IOException e) {
                System.out.println("Введены допустимые символы");
            }
            //if(checkRightInput(tmp, patternIdPassport))
            return tmp;
        }
    }

    private Gender inputGender() {
        while (isProcess) {

            String tmp = null;
            int choise = 0;
            System.out.println("Выберите пол клиента");
            System.out.println("1) Мужчина");
            System.out.println("2) Женщина\n-->");
            try {
                tmp = br.readLine();
                choise = Integer.parseInt(tmp);

                if (choise == 1) return Gender.MALE;
                if (choise == 2) return Gender.FEMALE;
            } catch (NumberFormatException e) {
                System.out.println("Введены допустимые символы");
            } catch (IOException e) {
                System.out.println("Введены допустимые символы");
            }
        }
    }

    private String inputEmailClient() {
        String tmp = null;
        while (isProcess) {
            System.out.println("Введите email клиента\n--> ");
            try {
                tmp = br.readLine();
            } catch (IOException e) {
                System.out.println("Введены допустимые символы");
            }
            if (checkRightInput(tmp, patternEmailClient))
                return tmp;
        }
    }

    private String inputPhoneNumberClient() {
        String tmp = null;
        while (isProcess) {
            System.out.println("Введите номер телефона клиента\n--> ");
            try {
                tmp = br.readLine();
            } catch (IOException e) {
                System.out.println("Введены допустимые символы");
            }
            //if(checkRightInput(tmp, patternEmailClient))
            return tmp;
        }
    }

    private double inputInitOverdraft() {
        String tmp = null;

        while (isProcess) {
            System.out.println("Введите начальный овердрафт\n--> ");
            try {
                tmp = br.readLine();
            } catch (IOException e) {
                System.out.println("Введены допустимые символы");
            }
            if (checkRightInput(tmp, patternInitOverdraft))
                return initOverdraft = Double.parseDouble(tmp);
        }
    }

    private boolean checkRightInput(String userInput, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(userInput);
        return m.matches();
    }

    //*******************
    //  FindClientCommand
    //*******************

    public void findClient() {
        String tmp = null;
        System.out.println("Ввеедите полное имя клиента (Ф И О)\n--> ");
        try {
            tmp = br.readLine();
        } catch (IOException e) {
            System.out.println("Введены допустимые символы");
        }
        Client c = getClient(bank, tmp);

        if (c != null) {
            System.out.println("Найден" + c.getClientSalutation() + " " + c.getFullName());
        }
    }

    //*******************
    //  GetAccountsCommand
    //*******************

    public void getAccountCommand() {
        String tmp = null;
        System.out.println("Ввеедите полное имя клиента (Ф И О)\n--> ");
        try {
            tmp = br.readLine();
        } catch (IOException e) {
            System.out.println("Введены допустимые символы");
        }
        for (Client c : bank.getClients()) {
            if (c.getFullName().equals(tmp)) {
                System.out.println("Найден\n");

                for (Account acc : c.getAccounts()) {
                    System.out.println(acc.maximumAmountToWithdraw());
                }
            }
        }
    }

    //*******************
    //  DepositCommand
    //*******************

    public void depositCommand() {
        String tmp = null;
        System.out.println("Ввеедите полное имя клиента (Ф И О)\n--> ");
        try {
            tmp = br.readLine();
        } catch (IOException e) {
            System.out.println("Введены допустимые символы");
        }
        for (Client c : bank.getClients()) {
            if (c.getFullName().equals(tmp)) {
                System.out.println("Найден\n");

                System.out.println("Введите сумму пополнения\n--> ");
                try {
                    tmp = br.readLine();
                    c.putToDeposit(Double.parseDouble(tmp));
                } catch (IOException e) {
                    System.out.println("Введены допустимые символы");
                }
            }
        }
    }

    //*******************
    //  WithdrawCommand
    //*******************

    public void withdrawCommand() {
        String tmp = null;
        System.out.println("Ввеедите полное имя клиента (Ф И О)\n--> ");
        try {
            tmp = br.readLine();
        } catch (IOException e) {
            System.out.println("Введены допустимые символы");
        }
        for (Client c : bank.getClients()) {
            if (c.getFullName().equals(tmp)) {
                System.out.println("Найден\n");

                System.out.println("Введите сумму снятия\n--> ");
                try {
                    tmp = br.readLine();
                    c.withdrawFromAccount(Double.parseDouble(tmp));
                } catch (OverDraftLimitExceededException e) {
                    System.out.println(e);
                } catch (NotEnoughFundsException e) {
                    System.out.println(e);
                } catch (IOException e) {
                    System.out.println("Введены допустимые символы");
                }
            }
        }
    }

    //*******************
    //  TransferCommand
    //*******************
    public void transferCommand() {
        String clientFrom = null;
        String clientTo = null;
        String money = null;
        System.out.println("Введите полное имя клиента для снятия средств (Ф И О)\n--> ");
        try {
            clientFrom = br.readLine();
        } catch (IOException e) {
            System.out.println("Введены допустимые символы");
        }
        for (Client c : bank.getClients()) {
            if (c.getFullName().equals(clientFrom)) {
                System.out.println("Найден\n");

                System.out.println("Введите сумму снятия\n--> ");
                try {
                    money = br.readLine();
                    c.withdrawFromAccount(Double.parseDouble(money));
                } catch (OverDraftLimitExceededException e) {
                    System.out.println(e);
                } catch (NotEnoughFundsException e) {
                    System.out.println(e);
                } catch (IOException e) {
                    System.out.println("Введены допустимые символы");
                }
            }
        }

        System.out.println("Введите полное имя клиента для пополнения (Ф И О)\n--> ");
        try {
            clientTo = br.readLine();
        } catch (IOException e) {
            System.out.println("Введены допустимые символы");
        }
        for (Client c : bank.getClients()) {
            if (c.getFullName().equals(clientTo)) {
                System.out.println("Найден\n");
                c.putToDeposit(Double.parseDouble(money));
            }
        }
    }

    ///////////////////////////////////////////////////////////////
    ///                 OTHERS METHODS
    ///////////////////////////////////////////////////////////////

    public void modifyBankWithdrow(Client client, double moneyOut) throws OverDraftLimitExceededException, NotEnoughFundsException {
        client.withdrawFromAccount(moneyOut);
    }

    public void modifyBankDeposit(Client client, double moneyIn) {
        client.putToDeposit(moneyIn);
    }

    public void printBalance(Client client) {
        System.out.println("\n*****************************************");
        System.out.println("* ---------------------------------------");
        System.out.println("* | Имя клиента: " + client.getClientSalutation());
        System.out.println("* | Баланс:      " + client.getBalance());
        System.out.println("* ---------------------------------------");
        System.out.println("*****************************************");
    }

    public void printMaximumAmountToWithdraw(Bank bank) {
        System.out.println("\n*****************************************");
        System.out.println("* ---------------------------------------");

        for (Client client : bank.getClients()) {
            System.out.print("* | Client Name:\t\t");
            System.out.println(client.getClientSalutation());

            for (Account acc : client.getAccounts()) {
                System.out.println("* | Total Amount Money: " + acc.maximumAmountToWithdraw());
            }
        }
        System.out.println("* ---------------------------------------");
        System.out.println("*****************************************");
    }

    public Client getClient(Bank bank, String clientName) {
        return bank.getClientMap().get(clientName);
    }
}
