package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotFoundException;
import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.exception.ClientNotFound;
import com.luxoft.bankapp.service.command.*;

import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;


public class BankCommander {

    public static Map<String, Command> commands = new TreeMap<String, Command>();
    public static Bank activebank;
    public static Client activeClient;

    static {
        commands.put("0", new Command() {                 // 0 - Exit Command
            public void execute() {
                System.exit(0);
            }

            public void printCommandInfo() {
                System.out.println("Выход");
            }
        }
        );
        commands.put("1", new FindClientCommand());
        commands.put("2", new GetAccountsCommand());
        commands.put("3", new DepositCommand());
        commands.put("4", new WithdrawCommand());
        commands.put("5", new TransferCommand());
        commands.put("6", new AddClientCommand());
    }

    public void registerCommand(String commandName, Command commandType) {
        commands.put(commandName, commandType);
    }

    public void removeCommand(String commandName) {
        if (commands.containsKey(commandName))
            commands.remove(commandName);
    }

    public static void main(String[] args) throws IOException, ClientExistsException, BankNotFoundException, ClientNotFound {


        //BankDataLoaderService ddd = new BankDataLoaderService();
        //ddd.loadFeed("feeds");
        //Анатолий Сергеевич Жуков

        //DBSelectBankCommander db = new DBSelectBankCommander();
        //db.execute("ОДЕССКАЯ ФИЛИЯ");
        //System.out.println( BankCommander.activebank.getBankName());

        //DBSelectClientCommander dbsc = new DBSelectClientCommander();
        //dbsc.execute(BankCommander.activebank,"Александр Сергеевич Пушкин");
        //System.out.println( BankCommander.activeClient.getFullName());

        if (args.length != 0) {
            if (args[0].equals("–report")) {
                BankReport bankReport = new BankReport();
                bankReport.getNumberOfClients(BankService.bank);
                bankReport.getAccountsNumber(BankService.bank);
                bankReport.getClientsSorted(BankService.bank);
                bankReport.getBankCreditSum(BankService.bank);
                bankReport.getClientsByCity(BankService.bank);
            }
        }

        while (true) {
            for (Map.Entry<String, Command> entry : commands.entrySet()) {
                System.out.print(entry.getKey() + ") ");
                entry.getValue().printCommandInfo();
            }
            System.out.print("Введите число--> ");
            Scanner sc = new Scanner(System.in);
            try {
                String command = sc.nextLine();
                commands.get(command).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


   /* static Command[] commands = {
            new Command() {                 // 0 - Exit Command
                public void execute()           { System.exit(0); }
                public void printCommandInfo()  { System.out.println("Exit"); }
            },
            new FindClientCommand(),        //1
            new GetAccountsCommand(),       //2
            new DepositCommand(),           //3
            new WithdrawCommand(),          //4
            new TransferCommand(),          //5
            new AddClientCommand()          //6
    };*/

// for (Map.Entry<String, String> entry : map.entrySet()){
//     System.out.println(entry.getKey() + "/" + entry.getValue());
// }

// Client client1 = new Client("Aaa Aaa Aaa",Gender.MALE, "aaaaa@aaa.com","+3809875780","AA999999", 300.00);
// Client client2 = new Client("Bbb Bbb Bbb",Gender.MALE, "bbbbb@bbb.com","+3809888888","BB999999", 600.00);
// Client client3 = new Client("Ccc Ccc Ccc",Gender.FEMALE,"cccc@ccc.com","+3809866666","СС999999", 900.00);

//bank.addClient(client1);
//bank.addClient(client2);
//bank.addClient(client3);

//client1.createAccount("checking");
//client2.createAccount("checking");
//client3.createAccount("checking");

    /* while (true) {
            for (int i = 0; i < BankCommander.commands.length; i++){ // show menu
                System.out.print(i + ") ");
                BankCommander.commands[i].printCommandInfo();
            }

            int     command = 0;
            boolean flag    = true;

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(System.in));

            while(flag){
                flag = true;
                try{
                    System.out.print("Введите число --> ");
                    String commandString = br.readLine();
                    command = Integer.parseInt(commandString);

                    if ( (command > 6) || (command < 0)){
                        System.out.println("Ошибка ввода");
                        continue;
                    }
                    flag = false;
                }catch (NumberFormatException e){ System.out.println("Ошибка ввода"); }
            }
            System.out.println(command);
            BankCommander.commands[command].execute();
        }*/