package com.luxoft.bankapp.service.JUnitTesting;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.CheckingAccount;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.Gender;
import com.luxoft.bankapp.exception.ClientExistsException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

public class TestServiceTest {
    Bank bank1, bank2;

    @Before
    public void initBanks() throws ClientExistsException {
        bank1 = new Bank("My Bank");
        bank1.setIdBank(1);
        Client client = new Client();
        client.setFullName("Ivan Ivanov");
        client.setCity("Kiev");
        client.setClientSalutation(Gender.FEMALE);
        // add some fields from Client
        // marked as @NoDB, with different values
        // for client and client2
        client.addAccount(new CheckingAccount());
        bank1.addClient(client);

        bank2 = new Bank("My Bank");
        bank2.setIdBank(1);
        Client client2 = new Client();
        client2.setFullName("Ivan Ivanov");
        client2.setCity("Kiev");
        client2.setClientSalutation(Gender.FEMALE);
        client2.addAccount(new CheckingAccount());
        bank2.addClient(client2);
    }

    @Test
    public void testEquals() throws IllegalAccessException {
        assertTrue(TestService.isEquals(bank1, bank2));
    }

}
