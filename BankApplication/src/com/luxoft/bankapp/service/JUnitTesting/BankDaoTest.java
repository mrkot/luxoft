package com.luxoft.bankapp.service.JUnitTesting;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.CheckingAccount;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.ClientExistsException;
import org.junit.Before;
import org.junit.Test;

class BankDaoTest {
    Bank bank;

    @Before
    public void initBank() throws ClientExistsException {
        bank = new Bank();
        bank.setBankName("My Bank");
        Client client = new Client();
        client.setFullName("Ivan Ivanov");
        client.setCity("Kiev");
        client.addAccount(new CheckingAccount());
        bank.addClient(client);
    }
    /*
    @Test
    public void testInsert() {
        BankDaoImpl.save(bank);

        Bank bank2 = BankDaoImpl.loadBank();

        assertTrue(TestService.compare(bank, bank2));
    }


    @Test
    public void testUpdate() throws ClientExistsException {
        BankDaoImpl.save(bank);

        // make changes to Bank
        Client client2 = new Client();
        client2.setName("Ivan Petrov");
        client2.setCity("New York");
        client2.addAccount(new SavingAccount());
        bank.addClient(new Client());
        BankDaoImpl.save(bank);

        Bank bank2 = BankDaoImpl.loadBank();

        assertTrue(TestService.compare(bank, bank2));
    }
    */
}

