package com.luxoft.bankapp.service.JUnitTesting;


import com.luxoft.bankapp.domain.bank.Bank;

import java.lang.reflect.Field;

public class TestService {

    public static boolean isEquals(Object o1, Object o2) throws IllegalAccessException {


        if ((o1 instanceof Bank) && (o2 instanceof Bank)) {

            Field[] fieldsObj1 = o1.getClass().getDeclaredFields();
            Field[] fieldsObj2 = o2.getClass().getDeclaredFields();

            for (int i = 0; i < fieldsObj1.length; i++) {

                fieldsObj1[i].setAccessible(true);
                fieldsObj2[i].setAccessible(true);

                if (!fieldsObj1[i].isAnnotationPresent(NoDB.class)) {

                    String str1 = fieldsObj1[i].get(o1).toString();
                    String str2 = fieldsObj2[i].get(o2).toString();

                    if (!str1.equalsIgnoreCase(str2)) return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
