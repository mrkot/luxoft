package com.luxoft.bankapp.domain.dao;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotFoundException;
import com.luxoft.bankapp.exception.ClientNotFound;

import java.util.List;

public interface ClientDAO {
    /**
     * Return client by its name, initialize client accounts.
     *
     * @param bank
     * @param name
     * @return
     */
    Client findClientByName(Bank bank, String name) throws ClientNotFound;

    /**
     * Returns the list of all clients of the Bank
     * and their accounts
     *
     * @param bank
     * @return
     */
    List<Client> getAllClients(Bank bank) throws BankNotFoundException;

    /**
     * Method should insert new Client (if id==null)
     * or update client in database
     *
     * @param client
     */
    void save(Client client);

    /**
     * Method removes client from Database
     *
     * @param client
     */
    void remove(Client client);
}
