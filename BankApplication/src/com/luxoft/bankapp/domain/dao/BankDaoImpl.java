package com.luxoft.bankapp.domain.dao;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.exception.BankNotFoundException;
import com.luxoft.bankapp.service.bank.BankCommander;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class BankDaoImpl extends BaseDao implements BankDAO {

    private static BankDaoImpl instance;

    private BankDaoImpl() {
    }

    public static BankDaoImpl getInstant() {
        if (instance == null) {
            instance = new BankDaoImpl();
        }
        return instance;
    }

    @Override
    public Bank getBankByName(String name) {
        Bank bank = new Bank(name);
        String sql = "SELECT ID FROM BANK WHERE NAME=?";

        PreparedStatement stmt;

        try {
            openConnection();

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, name);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id = rs.getInt("ID");
                bank.setIdBank(id);
                bank.setBankName(name);
                BankCommander.activebank = bank;
            } else {
                throw new BankNotFoundException(name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } catch (BankNotFoundException e) {
            System.out.println(e);
        } finally {
            closeConnection();
        }
        return bank;
    }
}
