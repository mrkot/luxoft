package com.luxoft.bankapp.domain.dao;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.exception.BankNotFoundException;

public interface BankDAO {
    /**
     * Finds Bank by its name.
     * Do not load the list of the clients.
     *
     * @param name
     * @return
     */
    Bank getBankByName(String name) throws BankNotFoundException;
}
