package com.luxoft.bankapp.domain.dao;

import com.luxoft.bankapp.domain.bank.*;
import com.luxoft.bankapp.exception.BankNotFoundException;
import com.luxoft.bankapp.exception.ClientNotFound;
import com.luxoft.bankapp.service.bank.BankCommander;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ClientDaoImpl extends BaseDao implements ClientDAO {

    private static ClientDaoImpl instance;

    private ClientDaoImpl() {
    }

    public static ClientDaoImpl getInstance() {
        if (instance == null) {
            instance = new ClientDaoImpl();
        }
        return instance;
    }

    @Override
    public Client findClientByName(Bank bank, String name) throws ClientNotFound {

        Set<Account> accounts = new HashSet<Account>();

        Client client = null;
        String sqlClient = "SELECT * FROM CLIENT WHERE NAME=? AND BANK_ID=?";
        String sqlAccount = "SELECT * FROM ACCOUNT WHERE CLIENT_ID=?";

        PreparedStatement stmtClient;
        PreparedStatement stmtAccount;

        try {
            openConnection();
            stmtClient = conn.prepareStatement(sqlClient);
            stmtClient.setString(1, name);
            stmtClient.setInt(2, bank.getIdBank());

            ResultSet rsClient = stmtClient.executeQuery();

            if (rsClient.next()) {

                int idClient = rsClient.getInt("ID");
                String fullName = rsClient.getString("NAME");

                Gender g;
                String gender = rsClient.getString("GENDER");
                if (gender.equals("MALE")) {
                    g = Gender.MALE;
                } else {
                    g = Gender.FEMALE;
                }

                String city = rsClient.getString("CITY");
                String passport = rsClient.getString("PASSPORT");
                String phone = rsClient.getString("PHONE");
                String email = rsClient.getString("EMAIL");


                String initOverdraft = rsClient.getString("INIT_OVERDRAFT");
                double initOverd = Double.parseDouble(initOverdraft);

                int idBank = rsClient.getInt("BANK_ID");

                client = new Client(fullName, g, email, phone, passport, initOverd);

                client.setIdClient(idClient);
                client.setIdBank(idBank);
            } else {
                throw new ClientNotFound(name);
            }

            //////////////////////////
            //получение счетов клиента
            //////////////////////////
            stmtAccount = conn.prepareStatement(sqlAccount);
            stmtAccount.setInt(1, client.getIdClient());

            ResultSet rsAccount = stmtAccount.executeQuery();

            while (rsAccount.next()) {

                int idAccount = rsAccount.getInt("ID");
                System.out.println(idAccount);
                String type = rsAccount.getString("TYPE");
                System.out.println(type);


                String balanceBD = rsAccount.getString("BALANCE");
                double balance = Double.parseDouble(balanceBD);

                String overdraftBD = rsAccount.getString("OVERDRAFT");
                double overdraft = Double.parseDouble(overdraftBD);

                int idClientAcc = rsAccount.getInt("CLIENT_ID");

                Account a = client.createAccount(type);

                if (type.equalsIgnoreCase("checking")) {
                    ((CheckingAccount) a).putToDeposit(balance);
                    ((CheckingAccount) a).setInitialOverdraft(client.getInitOverdraft());
                    ((CheckingAccount) a).setOverdraft(overdraft);
                } else {
                    ((SavingAccount) a).putToDeposit(balance);
                }
                client.addAccount(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            closeConnection();
        }

        BankCommander.activeClient = client;
        return client;
    }


    @Override
    public List<Client> getAllClients(Bank bank) {


        Client client = null;
        List<Client> listClients = new ArrayList<Client>();
        Set<Account> accounts = new HashSet<Account>();

        String sqlClient = "SELECT * FROM CLIENT WHERE BANK_ID=?";
        String sqlAccount = "SELECT * FROM ACCOUNT WHERE CLIENT_ID=?";

        PreparedStatement stmt;

        try {
            openConnection();
            stmt = conn.prepareStatement(sqlClient);

            stmt.setInt(1, bank.getIdBank());

            ResultSet rsClient = stmt.executeQuery();

            if (rsClient.next()) {

                int idClient = rsClient.getInt("ID");
                String fullName = rsClient.getString("NAME");

                Gender g;
                String gender = rsClient.getString("GENDER");
                if (gender.equals("MALE")) {
                    g = Gender.MALE;
                } else {
                    g = Gender.FEMALE;
                }

                String city = rsClient.getString("CITY");
                String passport = rsClient.getString("PASSPORT");
                String phone = rsClient.getString("PHONE");
                String email = rsClient.getString("EMAIL");

                BigDecimal initOverdraft = rsClient.getBigDecimal(20, 2);
                double initOverd = initOverdraft.doubleValue();

                int idBank = rsClient.getInt("BANK_ID");

                client = new Client(fullName, g, email, phone, passport, initOverd);

                client.setIdClient(idClient);
                client.setIdBank(idBank);

                //////////////////////////
                //получение счетов клиента
                //////////////////////////
                stmt = conn.prepareStatement(sqlAccount);
                stmt.setInt(1, idClient);

                ResultSet rsAccount = stmt.executeQuery();
                if (rsAccount.next()) {
                    int idAccount = rsAccount.getInt("ID");
                    String type = rsAccount.getString("TYPE");

                    BigDecimal balanceBD = rsAccount.getBigDecimal(20, 2);
                    double balance = balanceBD.doubleValue();

                    BigDecimal overdraftBD = rsAccount.getBigDecimal(20, 2);
                    double overdraft = overdraftBD.doubleValue();

                    int idClientAcc = rsAccount.getInt("CLIENT_ID");

                    Account a = client.createAccount(type);

                    if (type.equalsIgnoreCase("checking")) {
                        a.putToDeposit(balance);
                        ((CheckingAccount) a).setInitialOverdraft(initOverd);
                        ((CheckingAccount) a).setOverdraft(overdraft);
                    } else {
                        a.putToDeposit(balance);
                    }
                    client.addAccount(a);
                }
                listClients.add(client);
            } else {
                throw new BankNotFoundException("Нет такого банка");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } catch (BankNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return listClients;
    }

    @Override
    public void save(Client client) {

        String sqlDelAccount = "DELETE FROM ACCOUNT WHERE CLIENT_ID=?;";
        String sqlAddAccount = "INSERT INTO ACCOUNT(TYPE, BALANCE, OVERDRAFT, CLIENT_ID) VALUES('?', ?,?,?);";

        String sqlUpdateClient = "UPDATE CLIENT SET CITY='?', PHONE='?', EMAIL='?', INIT_OVERDRAFT=?, BANK_ID=? " +
                "WHERE ID=?;";

        Set<Account> accounts = new HashSet<Account>();
        accounts = client.getAccounts();

        PreparedStatement delAccount;
        PreparedStatement addAccount;
        PreparedStatement updClient;

        try {
            openConnection();
            //DELETE ACC
            delAccount = conn.prepareStatement(sqlDelAccount);
            delAccount.setInt(1, client.getIdClient());
            int rs = delAccount.executeUpdate();
            //ADD ACC
            addAccount = conn.prepareStatement(sqlAddAccount);
            for (Account acc : accounts) {
                ///
                if (acc instanceof CheckingAccount) {
                    addAccount.setString(1, "checking");
                } else {
                    addAccount.setString(1, "saving");
                }
                ///
                BigDecimal bigBalance = new BigDecimal(acc.getBalance());
                addAccount.setBigDecimal(2, bigBalance);
                ///
                if (acc instanceof CheckingAccount) {
                    BigDecimal bigOverdraft = new BigDecimal(((CheckingAccount) acc).getOverdraft());
                    addAccount.setBigDecimal(3, bigOverdraft);
                } else {
                    BigDecimal bigOverdraft = new BigDecimal(0.00);
                    addAccount.setBigDecimal(3, bigOverdraft);
                }
                ///
                addAccount.setInt(4, client.getIdClient());
                int result = addAccount.executeUpdate();
            }
            //UPDATE CLIENT
            updClient = conn.prepareStatement(sqlUpdateClient);

            updClient.setString(1, client.getCityClient());
            updClient.setString(2, client.getPhoneNumberClient());
            updClient.setString(3, client.getEmailClient());

            BigDecimal bigInitOverdraft = new BigDecimal(client.getInitOverdraft());
            updClient.setBigDecimal(4, bigInitOverdraft);

            updClient.setInt(5, client.getIdBank());
            updClient.setInt(6, client.getIdClient());

            int res = updClient.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.closeConnection();
        }
    }


    @Override
    public void remove(Client client) {

        String sqlDelClient = "DELETE FROM CLIENT WHERE ID='?';";
        String sqlDelAccount = "DELETE FROM ACCOUNT WHERE CLIENT_ID=?;";

        PreparedStatement delClient;
        PreparedStatement delAccount;

        try {
            openConnection();

            delAccount = conn.prepareStatement(sqlDelAccount);
            delAccount.setInt(1, client.getIdClient());
            int rs = delAccount.executeUpdate();

            delClient = conn.prepareStatement(sqlDelClient);
            delClient.setInt(1, client.getIdClient());
            int rs1 = delClient.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.closeConnection();
        }
    }
}
