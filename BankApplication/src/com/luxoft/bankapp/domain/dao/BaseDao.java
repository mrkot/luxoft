package com.luxoft.bankapp.domain.dao;

import java.sql.*;

public class BaseDao {
    Connection conn;

    public Connection openConnection() {
        try {
            Class.forName("org.h2.Driver"); // this is driver for H2
            conn = DriverManager.getConnection("jdbc:h2:C:\\db\\base", "sa", "");

            return conn;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
