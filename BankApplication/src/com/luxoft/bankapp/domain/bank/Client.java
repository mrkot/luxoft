package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

public final class Client implements Account, Comparable, Serializable {

    public static Account activeDbAccount;
    private String fullName;
    private Gender gender;
    private String city;
    private String emailClient;
    private String phoneNumberClient;
    private String idPassport;
    private double initOverdraft;
    private Set<Account> accounts = new HashSet<>();
    private Account activeAccount;
    //private static int id = 1;
    private int idClient;
    private int idBank;

    public Client() {
    }

    public Client(String name) {
        this.fullName = name;
    }

    public Client(String fullName, Gender gender, String emailClient,
                  String phoneNumberClient, String idPassport, double initOverdraft) {
        this.fullName = fullName;
        this.gender = gender;
        this.emailClient = emailClient;
        this.phoneNumberClient = phoneNumberClient;
        this.idPassport = idPassport;
        this.initOverdraft = initOverdraft;
        //this.idClient = id++;
    }

    public void parseFeed(Map<String, String> feed) {
        String accountType = feed.get("accounttype");
        Account acc = getAccount(accountType);
        acc.parseFeed(feed);
        gender.parseFeed(feed);
    }

    private Account getAccount(String accountType) {
        for (Account acc : accounts) {
            if (acc.getAccountType().equals(accountType)) return acc;
        }
        return createAccount(accountType);
    }


    public Account createAccount(String accountType) {
        if (accountType.equalsIgnoreCase("checking")) {
            activeAccount = new CheckingAccount(initOverdraft);
            accounts.add(activeAccount);
        } else {
            activeAccount = new SavingAccount();
            accounts.add(activeAccount);
        }
        return activeAccount;
    }

    public void putToDeposit(double x) {
        activeAccount.putToDeposit(x);
    }

    public void withdrawFromAccount(double x) throws NotEnoughFundsException {
        activeAccount.withdrawFromAccount(x);
    }


    public String getFullName() {
        return fullName;
    }

    public String getIdPassport() {
        return idPassport;
    }

    public String getClientSalutation() {
        return gender.getAppeal() + getFullName();
    }

    public double getBalance() {
        return activeAccount.getBalance();
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public String getCityClient() {
        return city;
    }

    public Account getActiveAccount() {
        return activeAccount;
    }

    public int getIdClient() {
        return idClient;
    }

    public String getPhoneNumberClient() {
        return phoneNumberClient;
    }

    public String getEmailClient() {
        return emailClient;
    }

    public double getInitOverdraft() {
        return initOverdraft;
    }

    public int getIdBank() {
        return idBank;
    }

    public void setActiveAccount(Account a) {
        this.activeAccount = a;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public void setIdBank(int idBank) {
        this.idBank = idBank;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setClientSalutation(Gender gender) {
        this.gender = gender;
    }

    public void addAccount(Account a) {
        accounts.add(a);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (obj instanceof Client) {
            Client client = (Client) obj;

            if (!(this.getFullName().equals(client.getFullName()))) return false;
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = result * prime * fullName.hashCode();
        result = result + prime * gender.hashCode();

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Имя:      ");
        sb.append(getClientSalutation());
        sb.append("\nПасспорт: ");
        sb.append(idPassport);
        sb.append("\nСчета:\n");

        for (Account account : accounts) {
            String tmp = account.toString();
            sb.append(account);
        }
        return sb.toString();
    }

    @Override
    public double maximumAmountToWithdraw() {
        return 0;
    }

    @Override
    public String getAccountType() {
        return null;
    }

    @Override
    public void decimalValue() {
    }

    @Override
    public int compareTo(Object o) {
        return this.getFullName().compareTo(((Client) o).getFullName());
    }
}
