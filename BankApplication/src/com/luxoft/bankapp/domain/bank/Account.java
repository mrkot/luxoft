package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.exception.OverDraftLimitExceededException;

import java.util.Map;

public interface Account {

    double getBalance();

    void putToDeposit(double x);

    double maximumAmountToWithdraw();

    void decimalValue();

    void withdrawFromAccount(double x) throws
            OverDraftLimitExceededException, NotEnoughFundsException;

    String getAccountType();

    void parseFeed(Map<String, String> feed);
}
