package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;

public abstract class AbstractAccount implements Account {

    protected double balance = 0;
    private static int idAccount = 1;

    protected AbstractAccount() {
        idAccount++;
    }

    public abstract double maximumAmountToWithdraw();

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public void putToDeposit(double moneyIn) {
        if (moneyIn < 0) {
            System.out.println("\nНевозможно положить отрицательную сумму\n");
            throw new IllegalArgumentException();
        }
        balance += moneyIn;
        System.out.println("\nПоступление на счет:    " + moneyIn + "\n");
    }

    public abstract void withdrawFromAccount(double x) throws NotEnoughFundsException;

    public int getIdAccount() {
        return idAccount;
    }

}
