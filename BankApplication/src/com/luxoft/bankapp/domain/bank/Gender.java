package com.luxoft.bankapp.domain.bank;

import java.io.Serializable;
import java.util.Map;

public enum Gender implements Serializable {
    MALE("Mr. "), FEMALE("Ms. ");

    private String appeal;

    Gender() {
    }

    Gender(String appeal) {
        this.appeal = appeal;
    }

    public String getAppeal() {
        return appeal;
    }


    public void parseFeed(Map<String, String> feed) {
        String gender = feed.get("gender");

        if (gender.equalsIgnoreCase("f")) {
            this.appeal = "FEMALE";
        } else {
            this.appeal = "MALE";
        }
    }
}