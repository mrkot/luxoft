package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.ClientExistsException;

import java.util.*;

public final class Bank {

    private Set<Client> clients = new TreeSet<>();
    private static List<ClientRegistrationListener> listeners = new ArrayList<>();
    private Map<String, Client> clientMap = new HashMap<>();
    //private static int id = 1;

    private int idBank;
    private String bankName;

    public Bank() {
    }

    public Bank(String bankName) {
        this.bankName = bankName;
        //this.idBank = id++;

        class PrintClientListener implements ClientRegistrationListener {
            @Override
            public void onClientAdded(Client c) {
                clientMap.put(c.getFullName(), c);
                System.out.println("\nAdded a new client: " + c.getClientSalutation());
            }
        }

        class EmailNotificationListener implements ClientRegistrationListener {
            @Override
            public void onClientAdded(Client c) {
                System.out.println("\nNotification email for client: "
                        + c.getClientSalutation() + " to be sent");
            }
        }

        class DebugListener implements ClientRegistrationListener {
            @Override
            public void onClientAdded(Client c) {
                System.out.println("* ---------------------------------------");
                System.out.println("* | Client Name:  " + c.getClientSalutation());
                System.out.println("* | Event Date:   " + new Date());
                System.out.println("* ---------------------------------------");
                System.out.println("*****************************************");
            }
        }

        listeners.add(new PrintClientListener());
        listeners.add(new EmailNotificationListener());
        listeners.add(new DebugListener());
    }


    //  name    bob
    public void parseFeed(Map<String, String> feed) {

        String name = feed.get("name"); // bob
        Client client = null;

        if (!clientMap.containsKey(name)) {
            client = new Client(name);
            clientMap.put(name, client);
        } else {
            client = clientMap.get(name);
        }
        client.parseFeed(feed);
    }


    public void addClient(Client client) throws ClientExistsException {
        String tmpName = client.getFullName();
        String tmpIdPassport = client.getIdPassport();

        for (Client c : clients) {
            if (c.getIdPassport().equalsIgnoreCase(tmpIdPassport)) {
                client = null;
                throw new ClientExistsException(tmpName);
            }
        }

        clients.add(client);

        for (ClientRegistrationListener listener : listeners) {
            listener.onClientAdded(client);
        }
    }

    public Set<Client> getClients() {
        return new TreeSet<>(clients);
    }

    public Map<String, Client> getClientMap() {
        return clientMap;
    }

    public void setIdBank(int id) {
        this.idBank = id;
    }

    public void setBankName(String name) {
        this.bankName = name;
    }

    public int getIdBank() {
        return idBank;
    }

    public String getBankName() {
        return bankName;
    }

}
