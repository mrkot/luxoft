package com.luxoft.bankapp.domain.bank;

public interface ClientRegistrationListener {
    void onClientAdded(Client c);
}
