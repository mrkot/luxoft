package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.OverDraftLimitExceededException;

import java.io.Serializable;
import java.util.Map;

public final class CheckingAccount extends AbstractAccount implements Account, Serializable {

    private double initialOverdraft;
    private int checkingId;
    private double overdraft;

    public CheckingAccount() {
    }

    public CheckingAccount(double initialOverdraft) {
        this.initialOverdraft = initialOverdraft;
        this.overdraft = initialOverdraft;
        checkingId = getIdAccount();
    }

    //овердрафт установленный банком
    public double getOverdraft() {
        return overdraft;
    }

    public void setInitialOverdraft(double initOverdraft) {
        this.initialOverdraft = initialOverdraft;
    }

    public void setOverdraft(double overdraft) {
        this.overdraft = overdraft;
    }

    public double getInitialOverdraft() {
        return initialOverdraft;
    }

    @Override
    public void withdrawFromAccount(double moneyOut) throws OverDraftLimitExceededException {

        if (moneyOut > (balance + initialOverdraft)) {
            throw new OverDraftLimitExceededException(moneyOut, balance, this);
        }

        if (moneyOut < 0) {
            System.out.println("\n*****************************************");
            System.out.println("* ---------------------------------------");
            System.out.println("Невозможно снять отрицательную сумму");
            System.out.println("* ---------------------------------------");
            System.out.println("*****************************************");
            throw new IllegalArgumentException();
        }

        System.out.println("\n*****************************************");
        System.out.println("* ---------------------------------------");
        System.out.println("* | Вы хотите снять: " + moneyOut);
        System.out.println("* ---------------------------------------");
        if (moneyOut <= balance) {
            balance -= moneyOut;
        } else {
            initialOverdraft = initialOverdraft - (moneyOut - balance);
            balance = 0;
        }
        System.out.println("* ---------------------------------------");
        System.out.println("* |	Ваш баланс составляет:    " + balance);
        System.out.println("* |	Ваш овердрафт составляет: " + initialOverdraft);
        System.out.println("* ---------------------------------------");
        System.out.println("*****************************************");
    }

    @Override
    public double maximumAmountToWithdraw() {
        return balance + initialOverdraft;
    }

    @Override
    public void decimalValue() {
        System.out.println(Math.round(balance));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (obj instanceof CheckingAccount) {
            CheckingAccount acc = (CheckingAccount) obj;

            if (this.checkingId == acc.checkingId) return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        return result * prime * checkingId;
    }

    @Override
    public String toString() {

        String str = "Поточный счет №:       " + checkingId +
                "\nБаланс составляет:      " + balance +
                "\nОвердрафт составляет    " + initialOverdraft + "\n";
        return str;
    }

    @Override
    public String getAccountType() {
        return "c";
    }

    @Override
    public void parseFeed(Map<String, String> feed) {
        String balanceClient = feed.get("balance");
        String overdraftClient = feed.get("overdraft");
        this.balance = Double.parseDouble(balanceClient);
        this.overdraft = Double.parseDouble(overdraftClient);
    }
}
