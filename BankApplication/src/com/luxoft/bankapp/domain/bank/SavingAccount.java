package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;

import java.io.Serializable;
import java.util.Map;

public final class SavingAccount extends AbstractAccount implements Account, Serializable {

    private int savingId;

    public SavingAccount() {
        savingId = getIdAccount();
    }

    @Override
    public void withdrawFromAccount(double moneyOut) throws NotEnoughFundsException {
        if (moneyOut < 0) {
            System.out.println("\n*****************************************");
            System.out.println("* ---------------------------------------");
            System.out.println("Невозможно снять отрицательную сумму");
            System.out.println("* ---------------------------------------");
            System.out.println("*****************************************");
            throw new IllegalArgumentException();
        }

        if (moneyOut > balance) {
            throw new NotEnoughFundsException(moneyOut, balance);
        }

        balance -= moneyOut;

        System.out.println("\n*****************************************");
        System.out.println("* ---------------------------------------");
        System.out.println("* | Вы сняли:              " + moneyOut);
        System.out.println("* ---------------------------------------");
        System.out.println("* ---------------------------------------");
        System.out.println("* | Ваш баланс составляет: " + balance);
        System.out.println("* ---------------------------------------");
        System.out.println("*****************************************");
    }

    @Override
    public double maximumAmountToWithdraw() {
        return balance;
    }

    @Override
    public void decimalValue() {
        System.out.println(Math.round(balance));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (obj instanceof SavingAccount) {
            SavingAccount acc = (SavingAccount) obj;

            if (this.savingId == acc.savingId) return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = result * prime * savingId;

        return result;
    }

    @Override
    public String toString() {
        String str = "Сберегательный счет №: " + savingId +
                "\nБаланс составляет:      " + balance + "\n";

        return str;
    }

    @Override
    public String getAccountType() {
        return "s";
    }

    @Override
    public void parseFeed(Map<String, String> feed) {
        String balanceClient = feed.get("balance");
        this.balance = Double.parseDouble(balanceClient);
    }

}
