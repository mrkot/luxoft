package com.luxoft.bankapp.exception;

public class NotEnoughFundsException extends BankException {


    public NotEnoughFundsException(double moneyOut, double moneyTotal) {

        output.append("\n");
        output.append("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        output.append("Невозможно снять: ");
        output.append(moneyOut);
        output.append("\n");
        output.append("Максимально доступная сумма: ");
        output.append(moneyTotal);
        output.append("\n");
        output.append("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");

    }

    @Override
    public String toString() {
        return output.toString();
    }
}
