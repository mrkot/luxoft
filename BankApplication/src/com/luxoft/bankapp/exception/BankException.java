package com.luxoft.bankapp.exception;

public abstract class BankException extends Exception {

    protected StringBuilder output;

    {
        output = new StringBuilder();
    }
}
