package com.luxoft.bankapp.exception;

public final class ClientExistsException extends BankException {


    public ClientExistsException(String name) {
        output.append("\n");
        output.append("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        output.append("Клиент: ");
        output.append(name);
        output.append(" Уже существует");
        output.append("\n");
        output.append("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    }

    @Override
    public String toString() {
        return output.toString();
    }
}
