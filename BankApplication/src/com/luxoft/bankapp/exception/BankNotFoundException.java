package com.luxoft.bankapp.exception;

public class BankNotFoundException extends BankException {


    public BankNotFoundException(String name) {

        output.append("Банк с именем ");
        output.append(name);
        output.append(" В базе отсутствует\n");
    }

    @Override
    public String toString() {
        return output.toString();
    }
}
