package com.luxoft.bankapp.exception;

import com.luxoft.bankapp.domain.bank.Account;

public final class OverDraftLimitExceededException extends NotEnoughFundsException {

    private Account account;
    private double moneyTotal;

    public OverDraftLimitExceededException(double moneyOut, double moneyTotal, Account account) {
        super(moneyOut, moneyTotal);
        this.account = account;
        this.moneyTotal = moneyTotal;

        output.append("\n");
        output.append("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        output.append("Невозможно снять: ");
        output.append(moneyOut);
        output.append("\n");
        output.append("Максимально доступная сумма: ");
        output.append(moneyTotal);
        output.append("\n");
        output.append("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    }

    @Override
    public String toString() {
        return output.toString();
    }
}

/*



 */