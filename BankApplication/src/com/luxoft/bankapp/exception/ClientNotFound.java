package com.luxoft.bankapp.exception;

public class ClientNotFound extends BankException {

    public ClientNotFound(String name) {
        output.append("Клиента с именем ");
        output.append(name);
        output.append(" нет в базе");

    }

    @Override
    public String toString() {
        return output.toString();
    }
}
